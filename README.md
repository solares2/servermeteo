# Node.js Rest APIs with Express & MySQL

Create mysqldatabase meteo:

launch this sql:
docs/meteo_master_db.sql


## Project setup
```
npm install
```

### Run
```
node server.js
```
