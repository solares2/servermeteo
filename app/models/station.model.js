const sql = require("./db.js");

// constructor
const Stations = function(station) {
  this.name = station.name;
  this.longitude = station.longitude;
  this.latitude = station.latitude;
};

Stations.create = (newStation, result) => {
  sql.query("INSERT INTO tbl_mast_meteo_stations SET ?", newStation, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created station: ", { id: res.insertId, ...newStation });
    result(null, { id: res.insertId, ...newStation });
  });
};

Stations.findById = (id, result) => {
  sql.query(`SELECT * FROM tbl_mast_meteo_stations WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found station: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Station with the id
    result({ kind: "not_found" }, null);
  });
};

Stations.getAll = result => {
  sql.query("SELECT * FROM tbl_mast_meteo_stations", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("stations: ", res);
    result(null, res);
  });
};

Stations.updateById = (id, station, result) => {
  sql.query(
    "UPDATE tbl_mast_meteo_stations SET name = ?, latitude = ?, longitude = ? WHERE id = ?",
    [station.name, station.latitude, station.longitude, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found station with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated station: ", { id: id, ...station });
      result(null, { id: id, ...station });
    }
  );
};

Stations.remove = (id, result) => {
  sql.query("DELETE FROM tbl_mast_meteo_stations WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Stations with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted station with id: ", id);
    result(null, res);
  });
};

Stations.removeAll = result => {
  sql.query("DELETE FROM tbl_mast_meteo_stations", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} stations`);
    result(null, res);
  });
};

Stations.findData = (id, result) => {
  sql.query(`SELECT dt.id,
          dt.data AS valor,
          v.unit AS unidades,
          v.name
       FROM tbl_mast_meteo_stations  st  ,
           tbl_mast_meteo_stations_data dt,
           tbl_mast_meteo_variables v
      WHERE st.id=dt.idstation AND
            v.id=dt.idvariable AND
        st.id=  ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found data: ", res[0]);
      result(null, res);
      return;
    }

    // not found Station with the id
    result({ kind: "not_found" }, null);
  });
};

module.exports = Stations;
