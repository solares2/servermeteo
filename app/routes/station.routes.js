module.exports = app => {
  const stations = require("../controllers/station.controller.js");

  // Create a new Station
  app.post("/stations", stations.create);

  // Retrieve all Stations
  app.get("/stations", stations.findAll);

  // Retrieve a single Station with id
  app.get("/stations/:id", stations.findOne);

    // Retrieve a data from Station with id
    app.get("/stationsData/:id", stations.findData);

  // Update a Station with id
  app.put("/stations/:id", stations.update);

  // Delete a Station with id
  app.delete("/stations/:id", stations.delete);

  // Create a new Station
  app.delete("/stations", stations.deleteAll);
};
