const Station = require("../models/station.model.js");

// Create and Save a new Station
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a Station
  const station = new Station({
    name: req.body.name,
    latitude: req.body.latitude,
    longitude: req.body.longitude
  });

  // Save Station in the database
  Station.create(station, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Station."
      });
    else res.send(data);
  });
};

// Retrieve all Stations from the database.
exports.findAll = (req, res) => {
  Station.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving stations."
      });
    else res.send(data);
  });
};

// Find a single Station with a id
exports.findOne = (req, res) => {
  Station.findById(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Station with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Station with id " + req.params.id
        });
      }
    } else res.send(data);
  });
};

// Update a Station identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  console.log(req.body);

  Station.updateById(
    req.params.id,
    new Station(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Station with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Station with id " + req.params.id
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a Station with the specified id in the request
exports.delete = (req, res) => {
  Station.remove(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Station with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Station with id " + req.params.id
        });
      }
    } else res.send({ message: `Station was deleted successfully!` });
  });
};

// Delete all Stations from the database.
exports.deleteAll = (req, res) => {
  Station.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all stations."
      });
    else res.send({ message: `All Stations were deleted successfully!` });
  });
};


// Find a single Station with a id
exports.findData = (req, res) => {
  Station.findData(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found data with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving data with id " + req.params.id
        });
      }
    } else res.send(data);
  });
};